# CoreOS Builder

This repository hosts a build script ([build.bash]) that will create a custom
[ISO] file for [Fedora CoreOS] that can be used to install CoreOS on the cloud,
a Virtual Machine, or even bare metal. The only dependencies required to run
the script are either [Podman] or [Docker].

# Getting Started

To get started, determine where you're going to run the script:

- Local Machine
  - Then all you need to do is clone this repository and make the changes
    required.
- CI/CD Pipeline
  - This is a little trickier since it'll require you to check in some files
    that *may* be sensitive. My suggestion would be to fork a private
    repository to store your files and configuration in.

The general process is to build the runtime container, then build the ISO file.

To build the runtime container:

1. If you require any certificates to be used in your container (i.e. if you're
   behind a corporate network), place them into the [certs] folder. They will
   be copied into the container for you.
2. Build the [Containerfile] with either `docker` or `podman`, i.e.
   ```bash
   podman build --force-rm --squash -t 'coreos-builder/runtime:latest' '.'
   ```

The image will now be available for use on your local machine.

To build the ISO file:

1. Edit the [config.bash] file to your liking. Each variable is commented
   appropriately.
2. Run the `build.bash` script via the runtime (binding the current directory),
   i.e.
   ```bash
   podman run                              \
           --rm                            \
           --userns 'keep-id'              \
           -v "${PWD}:/pwd:Z"              \
           "coreos-builder/runtime:latest" \
           ./build.bash
   ```

The resulting ISO file will be copied to the current directory and ready for
use.

Remember to check in any needed files (i.e. your public key file and
certificates) if you're building from a CI/CD pipeline. For an example of using
a CI/CD pipeline, I've included [gitlab-ci.yml]. This builds and pushes the
runtime image, then builds the ISO using it, storing the resulting ISO as an
artifact of the build.

During the build process a *cache* directory is created to store generated and
downloaded files. This is to speed up consecutive builds; however, if you'd
like/need to start fresh simply delete the `cache` directory. The build script
will recreate it on start.

[BASH]: https://www.gnu.org/software/bash/ "BASH Homepage."
[build.bash]: build.bash "The build script."
[certs]: certs "The certificates folder."
[config.bash]: config.bash "The build config file."
[Containerfile]: Containerfile "The runtime containerfile."
[Docker]: https://www.docker.com/ "The Docker Homepage."
[Fedora CoreOS]: https://fedoraproject.org/coreos/ "The Fedora CoreOS Homepage."
[gitlab-ci.yml]: gitlab-ci.yml "GitLab CI/CD configuration."
[ISO]: https://en.wikipedia.org/wiki/Optical_disc_image "Wikipedia entry for ISO."
[Podman]: https://podman.io/ "The Podman Homepage."
