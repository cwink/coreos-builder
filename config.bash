#!/bin/bash

# should point to the location of a certificate that exists, one that
# doesn't exist, or nothing (the latter 2 cases being if one isn't needed)
CERTFILE=''

# will be the hostname of the destination system
COREOSHOST='coreos'

# list of packages from the Fedora repository to install
PACKAGES=('buildah' 'golang-oras' 'neovim')

# location of the public key file to add for the core user
PUBKEYFILE=''

# specifies a port number where any port number equal to or greater than that
# port number can be open by any user on the system
UNPRIVPORT=80
