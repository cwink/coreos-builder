#!/bin/bash

set -e -o pipefail

trap cleanup EXIT SIGHUP SIGINT SIGTERM

cleanup()
{
	local err="${?}"
	if [[ ${err} -eq 0 ]]; then
		loginfo 'iso successfully built'
	else
		logerr 'failed to build iso'
	fi
}

logerr()
{
	command -v tput >'/dev/null' 2>&1 && tput setaf 1 1>&2
	printf "error: " 1>&2
	# shellcheck disable=SC2059
	printf "${@}" 1>&2
	printf "\n" 1>&2
	command -v tput >'/dev/null' 2>&1 && tput sgr0 1>&2
	return 0
}

loginfo()
{
	command -v tput >'/dev/null' 2>&1 && tput setaf 3
	printf "info: "
	# shellcheck disable=SC2059
	printf "${@}"
	printf "\n"
	command -v tput >'/dev/null' 2>&1 && tput sgr0
	return 0
}

loginfo 'sourcing configuration'
source 'config.bash'

cachedir='cache'
pubkeycont="$(cat "${PUBKEYFILE}")"

loginfo "creating cache folder"
mkdir -p "${cachedir}"

m4args=(
	"-DCOREOSHOST=${COREOSHOST}"
	"-DPACKAGES=${PACKAGES[*]}"
	"-DPUBKEYCONT=${pubkeycont}"
	"-DUNPRIVPORT=${UNPRIVPORT}"
)

if [[ -f ${CERTFILE} ]]; then
	cp -f "${CERTFILE}" "${cachedir}/ca-bundle.crt"
	m4args+=("-DHASCERT")
fi

loginfo "generating dest butane file"
m4 "${m4args[@]}" 'dest.bu.m4' >"${cachedir}/dest.bu"

loginfo 'generating dest ignition file'
butane -d '.' -o "${cachedir}/dest.ign" -ps "${cachedir}/dest.bu"

isoargs=('--dest-ignition' "${cachedir}/dest.ign" '--dest-device' '/dev/sda')

if [[ -f ${CERTFILE} ]]; then
	loginfo "generating live butane file"
	cat <<-EOF >"${cachedir}/live.bu"
		variant: fcos
		version: 1.5.0
		storage:
		  files:
		    - path: /etc/pki/ca-trust/source/anchors/ca-bundle.crt
		      contents:
		        local: ${cachedir}/ca-bundle.crt
	EOF
	loginfo 'generating live ignition file'
	butane -d '.' -o "${cachedir}/live.ign" -ps "${cachedir}/live.bu"
	isoargs+=('--ignition-ca' "${cachedir}/ca-bundle.crt")
	isoargs+=('--live-ignition' "${cachedir}/live.ign")
fi

isofile="$(echo 'cache/fedora-coreos-'*'.iso')"

if [[ -f ${isofile} ]]; then
	loginfo 'found existing iso %s' "${isofile}"
	loginfo 'resetting iso'
	coreos-installer iso reset "${isofile}"
else
	loginfo 'downloading iso'
	isofile="$(coreos-installer download -C "${cachedir}" -f 'iso')"
fi

loginfo 'customizing iso'
coreos-installer iso customize "${isoargs[@]}" "${isofile}"

loginfo 'copying iso to current directory'
cp -f "${isofile}" 'coreos.iso'
