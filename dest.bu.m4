variant: fcos
version: 1.5.0
passwd:
  users:
    - name: core
      ssh_authorized_keys:
        - PUBKEYCONT
storage:
  files:
    - path: /etc/hostname
      contents:
        inline: COREOSHOST
      overwrite: true
    - path: /etc/sysctl.d/10-port.conf
      contents:
        inline: net.ipv4.ip_unprivileged_port_start = UNPRIVPORT
    - path: /var/usrlocal/bin/rpm-ostree-install.bash
      mode: 0755
      contents:
        inline: |
          `#'!/bin/bash

          set -e -o pipefail

          trap cleanup EXIT SIGHUP SIGINT SIGTERM

          cleanup()
          {
                  mv '/tmp/fedora-'*'.repo' '/etc/yum.repos.d/'
          }

          mv '/etc/yum.repos.d/fedora-'*'.repo' '/tmp/'

          rpm-ostree install -y --allow-inactive "${@}"
ifdef(`HASCERT',dnl
    - path: /etc/pki/ca-trust/source/anchors/ca-bundle.crt
      contents:
        local: cache/ca-bundle.crt
    - path: /etc/environment
      overwrite: true
      contents:
        inline: NODE_EXTRA_CA_CERTS='/etc/pki/ca-trust/source/anchors/ca-bundle.crt'
)dnl
systemd:
  units:
    - name: zincati.service
      enabled: false
    - name: rpm-ostree-install.service
      enabled: true
      contents: |
        [Unit]
        Description=Install extra packages with rpm-ostree.

        After=network-online.target
        Before=zincati.service
        Requires=coreos-update-ca-trust.service
        Wants=network-online.target

        ConditionPathExists=!/var/lib/%N.stamp

        [Service]
        Type=oneshot
        RemainAfterExit=yes
        ExecStart=/var/usrlocal/bin/rpm-ostree-install.bash PACKAGES
        ExecStart=/bin/touch "/var/lib/%N.stamp"
        ExecStart=/bin/systemctl --no-block reboot

        [Install]
        WantedBy=multi-user.target
